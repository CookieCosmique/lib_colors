# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bperreon <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/04 10:55:37 by bperreon          #+#    #+#              #
#    Updated: 2015/12/14 14:46:21 by bperreon         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = lib_colors.a
CC = gcc
CFLAGS = -Wall -Wextra -Werror -O3
SOURCE = ./src/new_color.c ./src/hsl_to_rgb.c
HEADER = -I./include/
OBJ = $(SOURCE:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

%.o: %.c include
	$(CC) $(CFLAGS) $(HEADER) -c $< -o $@

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
