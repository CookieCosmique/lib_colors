/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_colors.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/15 17:06:04 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIB_COLOR_H
# define LIB_COLOR_H

# include <math.h>

typedef struct	s_color
{
	double	r;
	double	g;
	double	b;
}				t_color;

t_color			new_color(const double r, const double g, const double b);

t_color			hsl_to_rgb(const double h, const double s, const double l);

#endif