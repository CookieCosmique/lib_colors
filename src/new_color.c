/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_color.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/15 17:06:04 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_colors.h"

t_color		new_color(const double r, const double g, const double b)
{
	t_color		new;

	new.r = r;
	new.g = g;
	new.b = b;
	return (new);
}
