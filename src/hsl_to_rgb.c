/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hsl_to_rgb.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 13:43:05 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/15 17:06:04 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_colors.h"


static t_color		get_color_p(int h, float c, float x)
{
	t_color		color;

	if (h < 1)
		color = new_color(c, x, 0);
	else if (h < 2)
		color = new_color(x, c, 0);
	else if (h < 3)
		color = new_color(0, c, x);
	else if (h < 4)
		color = new_color(0, x, c);
	else if (h < 5)
		color = new_color(x, 0, c);
	else if (h < 6)
		color = new_color(c, 0, x);
	else
		color = new_color(0, 0, 0);
	return (color);
}


static double		moded(double a, double b)
{
	if (b == 0)
		return (0);
	return (a - (b * (int)(a / b)));
}

t_color			hsl_to_rgb(const double h, const double s, const double l)
{
	t_color		color;
	double		c;
	double		h_p;
	double		x;
	t_color		color_p;
	double		m;

	c = (1.0 - fabs(2.0 * l - 1.0)) * s;
	h_p = h / 60.0;
	x = c * (1.0 - fabs(moded(h_p, 2) - 1));
	color_p = get_color_p(h_p, c, x);
	m = l - c / 2;
	color = new_color(color_p.r + m, color_p.g + m, color_p.b + m);
	return (color);
}
